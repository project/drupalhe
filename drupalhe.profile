<?php

/* Hebrew Drupal installation profile */

/* Written by Yuval Hager <yuval@avramzon.net>, Feb 2007
 * Work sponsored by Linnovate Ltd. <info@linnovate.net>
 * 
 * Licensed under GPL V.2
 */

/**
 * Return an array of the modules to be enabled when this profile is installed.
 *
 * @return
 *  An array of modules to be enabled.
 */
function drupalhe_profile_modules() {
  return array('block', 'color', 'comment', 'filter', 'help', 'menu', 'node', 'system', 'taxonomy', 'user', 'watchdog', 'locale', 'autolocale');
}

/**
 * Return a description of the profile for the initial installation screen.
 *
 * @return
 *   An array with keys 'name' and 'description' describing this profile.
 */
function drupalhe_profile_details() {
  return array(
    'name' => 'דרופל עברי',
    'description' => 'בחר תצורה זו כדי להתקין את דרופל בצורה המותאמת ביותר עבור אתר עברי'
  );
}

/**
 * Uses functionality in autolocale.install to import PO files
 */
function drupalhe_install() {
  _autolocale_install_po_files();
}

/**
 * Perform any final installation tasks for this profile.
 *
 * @return
 *   An optional HTML string to display to the user on the final installation
 *   screen.
 */
function drupalhe_profile_final() {
  // Insert default user-defined node types into the database.
  $common = array(
    'module' => 'node',
    'custom' => TRUE,
    'modified' => TRUE,
    'locked' => FALSE,
    'has_body' => TRUE,
    'body_label' => st('Body'),
    'has_title' => TRUE,
    'title_label' => st('Title'),
  );
  $types = array(
    array_merge(
      array(
        'type' => 'page',
        'name' => st('Page'),
        'description' => st('If you want to add a static page, like a contact page or an about page, use a page.')
      ), 
      $common
    ),
    array_merge(
      array(
        'type' => 'story',
        'name' => st('Story'),
        'description' => st('Stories are articles in their simplest form: they have a title, a teaser and a body, but can be extended by other modules. The teaser is part of the body too. Stories may be used as a personal blog or for news articles.')
      ),
      $common
    ),
  );

  foreach ($types as $type) {
    $type = (object) _node_type_set_defaults($type);
    node_type_save($type);
  }

  // Default page to not be promoted and have comments disabled.
  variable_set('node_options_page', array('status'));
  variable_set('comment_page', COMMENT_NODE_DISABLED);

  // Don't display date and author information for page nodes by default.
  $theme_settings = variable_get('theme_settings', array());
  $theme_settings['toggle_node_info_page'] = FALSE;
  variable_set('theme_settings', $theme_settings);

  // Mimic system_themes_submit
  system_theme_data(); /* collect available themes */
  db_query("UPDATE {system} SET status = 0 WHERE type = 'theme'");
  $themes = array('garland','garlandrtl');
  foreach ($themes as $theme) {
      system_initialize_theme_blocks($theme);  
      db_query("UPDATE {system} SET status = 1 WHERE type = 'theme' and name = '%s'", $theme);
  }
  variable_set('theme_default','garlandrtl');

  /* Set default site name */
  variable_set('site_name', 'דרופל');
}
