This is a Hebrew profile for Drupal installation.

In order to utilize it fully, one should follow the steps below:

== Install Drupal ==
* Download and extract drupal-5.1
* Create the directories 'sites/all/modules' and 'sites/all/themes'

== Copy Installation Profile ==

* Copy this directory onto the 'profiles/' directory
* Rename the file 'he.po.txt' to 'he.po'

== The autolocale module ==

* Download from 'http://drupal.org/project/autolocale' (NOTE: currently, there is no available download link on that page. You will have to download the module from the CVS until this is fixed)
* Copy the autolocale directory onto 'sites/all/modules'

== Hebrew translation ==
* Download and extract latest Hebrew translation from 'http://drupal.org/project/he'
* Copy the file he.po from the translation onto 'sites/all/modules/autolocale/po/drupal-<version>.he.po'
* Copy the file installer.po onto 'sites/all/modules/autolocale/po/installer-<version>.he.po'

== Garlandrtl theme ==
* Download the theme from 'http://drupal.org/project/garlandrtl'
* Copy the garlandrtl directory onto 'sites/all/themes/'

== Final fixes ==
This last step adds a few rtl-css files for use during the installation, and also make sure Hebrew is automatically chosen and drupalhe profile upon installation

* run the following from the drupal main directory:

patch -p1 < profiles/drupalhe/drupalhe.diff.txt

(For installation on a system other than Un*x, consult Google on how to apply
a diff file).

